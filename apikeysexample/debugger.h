//
//  debugger.h
//  apikeysexample
//
// Disable debugger routines
// from http://www.splinter.com.au/2014/09/16/storing-secret-keys/

#ifndef debugger_h
#define debugger_h

#include <stdio.h>

void disable_gdb();

#endif /* debugger_h */
